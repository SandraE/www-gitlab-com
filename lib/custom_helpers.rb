module CustomHelpers
  def icon(icon, cssclass = "", attrs = {})
    width = attrs[:width] || 76
    height = attrs[:height] || 76
    viewbox_width = attrs[:viewbox_width] || width
    viewbox_height = attrs[:viewbox_height] || height
    label = attrs[:label] || ""
    content_tag :svg, viewbox: "0 0 #{viewbox_width} #{viewbox_height}", width: width, height: height, class: cssclass, aria: {label: label}, role: "img" do
      partial "includes/icons/#{icon}.svg"
    end
  end

  def xml_feed_content(article)
    content = article.body

    if article.data.image_title
      content << "<img src='#{data.site.url}#{article.data.image_title}' class='webfeedsFeaturedVisual' style='display: none;' />"
    else
      content << "<img src='#{data.site.url}#{image_path("default-blog-image.png")}' class='webfeedsFeaturedVisual' style='display: none;' />"
    end

    h(content)
  end

  def markdown(text)
    # Scope parameter is necessary to make Markdown in YAML work properly
    # See: https://github.com/middleman/middleman/issues/653#issuecomment-9954111
    Tilt['markdown'].new { text }.render(scope: self)
  end

  def kramdown s
    Kramdown::Document.new(s).to_html
  end

  def open_jobs
    data.jobs.select(&:open).sort_by(&:title)
  end

  def vacancies
    data.vacancies.sort_by(&:position)
  end

  def vacancy_name(vacancy)
    name = ''
    name += vacancy.level if vacancy.level
    name += ' ' + vacancy.position
    name += ', ' + vacancy.specialist if vacancy.specialist
    name
  end

  def vacancy_link(vacancy)
    "<a href='/careers/#{vacancy_name(vacancy).downcase.parameterize}'>#{vacancy_name(vacancy)}</a>"
  end

  def job_for_current_page
    open_jobs.detect do |job|
      job_desc = job.description.sub(%r{\A/}, '').split('/')
      File.join(job_desc) == File.dirname(current_page.request_path)
    end
  end

  def salary_avail
    data.jobs.select(&:salary).sort_by(&:title)
  end

  def salary_for_current_job
    salary_avail.detect do |job|
      job.description.start_with?("/#{File.dirname(current_page.request_path)}")
    end
  end

  def font_url(current_page)
    fonts = ["Source+Sans+Pro:200,300,400,600"]

    if current_page.data.extra_font
      fonts = fonts.concat current_page.data.extra_font
    end
    fonts = fonts.join("|")

    "//fonts.googleapis.com/css?family=#{fonts}"
  end

  def highlight_active_nav_link(link_text, url, options = {})
    options[:class] ||= ""
    options[:class] << " active" if url == current_page.url
    link_to(link_text, url, options)
  end

  def full_url(current_page)
    "#{data.site.url}#{current_page.url}"
  end

  def current_version
    ReleaseList.new.release_posts.first.version
  end
end
