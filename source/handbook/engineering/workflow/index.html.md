---
layout: markdown_page
title: "Engineering Workflow"
---

This document explains the workflow for anyone working with issues in GitLab Inc.
For the workflow that applies to everyone please see [PROCESS.md](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md).

## On this page
{:.no_toc}

- TOC
{:toc}


## GitLab Flow

Products at GitLab are built using the [GitLab Flow](http://doc.gitlab.com/ee/workflow/gitlab_flow.html).

## Broken master

If you notice that the tests for the `master` branch of GitLab CE or EE are
failing (red) or broken (green as a false positive), fixing this takes priority
over everything else development related, since everything we do while tests are
broken may break existing functionality, or introduce new bugs and security
issues.

Create an issue, post about it in `#development` so that other developers are
aware of the problem and can help. If the problem cannot be fixed by you within
a few hours, `@mention` the relevant Engineering Leads and CTO in the issue and
on Slack, so that resources can be assigned to fix it as quickly as possible.

## Security issues

If you find or are alerted of a security issue, major or small, fixing this
takes priority over everything else development related.

Create a **confidential issue** mentioning the Security and the relevant
Engineering Leads, as well as the VP of Engineering, and post about it in
`#security`.

## Basics

1. Start working on an issue you’re assigned to. If you’re not assigned to any issue, find the issue with the highest priority you can work on, by relevant label. [You can use this query, which sorts by priority for the started milestones][priority-issues], and filter by the label for your team.
1. If you need to schedule something or prioritize it, apply the appropriate labels. See below for details.
1. You are responsible for the issue that you're assigned to. This means it has
  to ship with the milestone it's associated with. If you are not able to do
  this, you have to communicate it early to your manager. In teams, the team is
  responsible for this (see below).
1. You (and your team, if applicable) are responsible for:
  - ensuring that your changes [apply cleanly to GitLab Enterprise Edition][ce-ee-docs].
  - the testing of a new feature or fix, especially right after it has been
    merged and packaged.
1. Once a release candidate has been deployed to the staging environment, please
  verify that your changes work as intended. We have seen issues where bugs did
  not appear in development but showed in production (e.g. due to CE-EE merge
  issues).

For general guidelines about issues and merge requests, be sure to read the
[GitLab Workflow][gitlab-workflow].

[priority-issues]: https://gitlab.com/groups/gitlab-org/issues?scope=all&sort=priority&state=opened&milestone_title=%23started&assignee_id=0
[ce-ee-docs]: https://docs.gitlab.com/ee/development/limit_ee_conflicts.html
[gitlab-workflow]: /handbook/communication/#gitlab-workflow

## Working in Teams

For larger issues or issues that contain many different moving parts,
you'll be likely working in a team. This team will typically consist of a
[backend developer](https://about.gitlab.com/jobs/developer/), a
[frontend developer](https://about.gitlab.com/jobs/frontend-engineer/), a
[UX designer](https://about.gitlab.com/jobs/ux-designer/) and a
[product manager](https://about.gitlab.com/jobs/product-manager/).

1. Teams have a shared responsibility to ship the issue in the planned release.
    1. If the team suspects that they might not be able to ship something in
  time, the team should escalate / inform others as soon as possible.
  A good start is informing your lead.
    1. It's generally preferable to ship a smaller iteration of an issue,
  than ship something a release later.
1. Consider starting a Slack channel for a new team,
but remember to write all relevant information in the related issue(s).
You don't want to have to read up on two threads, rather than only one, and
Slack channels are not open to the greater GitLab community.

## Choosing something to work on

Start working on things with the highest priority in the current milestone. The
priority of items are defined under labels in the repository, but you are able
to sort by priority.

After sorting by priority, choose something that you’re able to tackle and falls
under your responsibility. That means that if you’re a frontend developer,
you work on something with the label `Frontend`.

To filter very precisely, you could filter all issues for:

- Milestone: Started
- Assignee: Unassigned
- Label: Your label of choice. For instance `CI`, `Discussion`, `Edge`, `Frontend`, or `Platform`
- Sort by priority

[Use this link to quickly set the above parameters][priority-issues]. You'll
still need to filter by the label for your own team.

If you’re in doubt about what to work on, ask your lead. They will be able to tell you.

## Workflow labels

Labels are described in our [Contribution guide][contrib-labels-guide].

[contrib-labels-guide]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#workflow-labels

## Getting data about GitLab.com

GitLab.com is a very large instance of GitLab Enterprise Edition.
It runs release candidates for new releases, and sees a lot of issues because of the amount of traffic it gets.
There are several internal tools available for developers at GitLab to get data about what's happening in the production system:

### Performance data

There is extensive [monitoring](https://monitor.gitlab.net/) publically available
for GitLab.com. For more on this and related tools, see the [monitoring
performance handbook](/handbook/infrastructure/monitoring).

More details on [GitLab Profiler](http://redash.gitlab.com/dashboard/gitlab-profiler-statistics)
are also found in the [monitoring performance handbook](/handbook/infrastructure/monitoring).

### Error reporting

- [Sentry](https://sentry.gitlap.com/) is our error reporting tool
- [log.gitlap.com](https://log.gitlap.com/) has production logs
- [prometheus.gitlab.com](https://prometheus.gitlab.com/alerts) has alerts for
  the [production team](/handbook/infrastructure/#production-team)

## Scheduling issues

GitLab Inc has to be selective in working on particular issues.
We have a limited capacity to work on new things. Therefore, we have to
schedule issues carefully. This is done primarily by product and
engineering managers.

Each issue that is scheduled should meet most of these criteria:

1. It should be in line with our [vision for GitLab](https://about.gitlab.com/direction/#vision)
1. It benefits our users
1. It is [technically viable](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#contribution-acceptance-criteria)
1. The technical complexity is acceptable (we want to preserve our ability to make changes quickly in the future so we try to avoid: complex code, complex data structures, and optional settings)
1. It is orthogonal to other features (prevent overlap with current and future features)
1. Its requirements are clear
1. It can be achieved within the scheduled milestone.
Larger issues should be split up, so that individual steps can be achieved
within a single milestone.

Direction issues are the big, prioritized new features for each release.
They are limited to a small number per release so that we have plenty of
capacity to work on other important issues, bug fixes, etc.

Issues that are not scheduled for a future milestone,
but we are committed to doing, are put in the Backlog milestone.

If you want to schedule an `Accepting Merge Requests` issue, please remove
the label first.

Any scheduled issue should have a team label assigned, and at least one type label.

### Requesting something to be scheduled

Only fleshed-out issues can be scheduled. If an issue is vague or has unclear requirements, we can not schedule it.

To request a scheduling of an issue, ask the responsible lead.
You can find the leads on the team page, and in the descriptions of the team labels.
For (major) feature requests, ask the relevant product manager. Right now this is either Mark (for CI) or Job.

We have much more requests for great features than we have capacity to work on.
There is a good chance we’ll not be able to work on something.
Make sure the appropriate labels (such as `customer`) are applied so every issue is given the priority it deserves.

### Scheduling issues into a milestone

Engineering and product schedule (establish scope of) which issues are to be worked on in the following milestone. In particular:
* Engineering leads are responsible for resource planning and allocation.
* Engineering leads are responsible for prioritizing bugs and tech debt.
* Product managers are responsible for prioritizing features, with feedback from all relevant stakeholders.
* Based on the above, engineering leads and product managers collaborate and establish scope by the **4th** of a month, for the release in the subsequent month,
according to the [scheduling timeline](https://about.gitlab.com/handbook/product/#scheduling-timeline-throughout-a-milestone).
* The entire process happens asynchronously, and is mediated through individual issues themselves.


### Process improvement

There is an informal scheduling process discussion in the #scheduling Slack channel.
Anyone can join and suggest improvements to our scheduling process.
