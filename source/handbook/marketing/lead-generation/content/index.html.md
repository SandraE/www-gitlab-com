---
layout: markdown_page
title: "Content"
---

## On this page
* [Campaigns](#campaigns)
* [Campaign Brief Process](#campaignbrief)
* [Webcasts](#webcasts)

## Campaigns<a name="campaigns"></a>
Leadgen is responsible for executing marketing campaigns for GitLab.  We define a campaign as any programmed interaction with a user, customer, or prospect.  For each campaign, we will create a campaign brief that outlines the overall strategy, goals, and plans for the campaign.

## Campaign Brief Process<a name="campaignbrief"></a>
To create a campaign brief, first start with the [campaign brief template](https://docs.google.com/a/gitlab.com/document/d/1GttZqr7sjuvP9kWuIPfbif2b2VyNJtbN8CbL4tKJX2Q/edit?usp=sharing).  Fill out all fields in the brief as completely as possible.  Certain fields might not be applicable to a particular campaign.  For example, an email nurture campaign leveraging text based emails won’t have a visual design component.  This field can be left blank in that example.

Once the campaign brief is filled out, create an issue in the GitLab Marketing project and link to the campaign brief.

On the GitLab issue, make sure to:
* Tag all stakeholders
* Use the Marketing Campaign label
* Set the appropriate due date (the due date should be the campaign launch date)
* If there are specific deliverables, create a todo list in the issue description for each stakeholder along with a due date

## Webcasts<a name="webcasts"></a>

Webcasts are one the greatest tools we have to communicate with our audience. 
GitLab webcasts aim to be informative, actionable, and interactive. 

### Best Practices

1. Give yourself **at least** 15 days of promotion. 
2. Send invitation emails 3 weeks out, 2 weeks out, 1 week out, and 2 hours before event. 
3. Only send promotional emails Tuesday, Wednesday, or Thursday for optimal results. 
4. Send reminder emails to registrants 1 week out, day before, and one hour before the event. 
5. Host webcasts on a Wednesday or Thursday. 
6. Post links to additional, related resources during the event. 
7. Include "contact us" information at the end of the presentation. 
8. Send the recording to all registrants, whether they attended or not. 
9. Publish a post-webcast blog post capturing some key insights to encourage on-demand registrations. 
10. Add webcast to the GitLab resources page. 

### Speaker Approval

The Lead Generation team depends on GitLab's subject matter experts to deliver webcast presentations. However, we must ensure that 
when we ask a speaker to participate on a webcast that the work is approved. Please use the following process when asking someone outside of marketing
to participate on a webcast.

1. Have an abstract of the content prepared before asking for a presenter. 
2. Send the abstract to both the proposed speaker and their manager to review. **A speaker is not considered booked unless they have approval from their manager.**
3. Address and resolve any concerns regarding the abstract.
4. Once the manager approves and the speaker accepts, you can move forward with the webcast. 

### Tips for Speakers

Here are some basic tips to help ensure that you have a good experience preparing for and presenting on a webcast.

#### Before committing
Ask us any questions you have about the time commitment etc. and what exactly our expectations are. Talk about it with your manager if you're on the fence about your availability, bandwidth, or interest. Make sure you're both on the same page. We want this to be a meaningful professional development exercise for you, not a favor to us that you're lukewarm about — if you feel that way, none of will be able to do our best job. We'll be honest with you, so please do the same for us. 

#### Before the dry run
Select and set up your presentation space. Pick a spot with good wifi, and we recommend setting up an external mic for better audio quality, although this is optional. If you will be presenting from your home, alert your spouse/roommates of the time/date & ask them to be out of the house if necessary. Depending on your preferences and comfort level with public speaking, run through the script several times.

#### Before the presentation
Try to get a good sleep the night before, and, if the presentation is in the morning, wake up early enough to run through your notes at least once. 

### Logistical set-up

1. Create webcast in On24
   - Once the webcast is created, capture the `Event ID` from the overview page.
   - Make sure to turn off all email notifications within On24 as these will be handled by Marketo
   - Under the `Registration` tag, under `Options`, check the `Enable Login Only Option`
2. Clone the most recent webcast in Marketo
   - Title the webcast in the following format: `YYYYMMDD_{Webcast Title}`. For example, 20170418_MovingToGit
   - On the webcast summary page, set the event partner with the following information:
      - Event Partner: ON24
      - Login: ON24
      - Event Id: The ID of the event from the On24 platform
3. Update `My Tokens` at the webcast program level
   - Update the add to calendar tokens
      - Create an event in Google Calendar and copy the link from `Publish Event`
      - Update the information in the iCal and Outlook calendar files (these will be identical)
   - Update the event date and time
   - Update the email body with the description of the webcast
4. Schedule the reminder emails
   - In the reminders folder, select each of the smart campaigns to send the emails
   - Under the `Smart List` tab, change the date of activity to not send the email if the user has registered for the webcast within the last 48 hours of when the email will send
   - Under the `Schedule` tab, schedule the email to go out at the appropriate time based on the smart campaign you are editing
5. Edit the landing page to have the appropriate webcast description, date, and time.
6. In On24, go to the `Console Builder` tab and edit the console to be GitLab branded and have all the requested widgets.
