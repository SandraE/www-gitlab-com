---
layout: markdown_page
title: "Objectives and Key Results (OKRs)"
---

## What are OKRs?

See [Wikipedia](https://en.wikipedia.org/wiki/OKR) and [Google Drive](https://docs.google.com/presentation/d/1ZrI2bP-XKEWDWsT-FLq5piuIwl84w9cYH1tE3X5oSUY/edit) (GitLab internal).

## Format

`  Owner: Key Result as a sentence. Metric`

- We use two spaces to indent instead of tabs.
- OKRs start with the owner of the key result. When referring to a team lead we don't write don't 'lead' since it is shorter and the team goal is the same.
- The key result can link to an issue.
- The metric can link to real time data about the current state.

## Levels

We only list your key results, these have your (team) name on them.
Your objectives are the key results under which your key results are nested , these should normally be owned by your manager.
We don't use [Lattice](https://latticehq.com/) because it duplicates the key results of your manager by creating separate objectives for yourself.
We do OKRs at them team level, we don't do [individual OKRs](https://hrblog.spotify.com/2016/08/15/our-beliefs/) unless it is a director level or higher position.
We have no more than [five layers in our team structure](https://about.gitlab.com/team/structure/).
Because we go no further than the team level we end up with a maximum 4 layers of indentation on this page (this layer count excludes the three top header with the main objectives for the company).
The match of one key result with the higher key result doesn't have to be perfect.
Every team should have at most 9 key results, to make counting easier only mention the team and people when they are the owner.
The advantage of this format is that the OKRs of the whole company will fit on 3 pages, making it much easier to have an overview.

## Updating

The key results are updated continually throughout the quarter when needed.
Everyone is welcome to a suggestion to improve them
To update make a merge request and assign it to the CEO.
If you're a [team member](https://about.gitlab.com/team/) or in the [core team](https://about.gitlab.com/core-team/) please post a link to the MR in the #ceo channel.

At the top of the OKRs is a link to the state of the OKRs at the start of the quarter so people can see a diff.

Timeline of how we draft the OKRs:

1. Executive team pushes updates to this page: 4 weeks before the start of the quarter
1. Executive team 90 minute planning meeting: 3 weeks before the start of the quarter
1. Discuss with the board and the teams: 2 weeks before the start of the quarter
1. Executive team 'how to achieve' presentations: 1 week before the start of the quarter
1. Add Key Results to top of 1:1 agenda's: before the start of the quarter
1. Present OKRs at a functional group update: first week of the quarter
1. Present 'how to achieve' at a functional group update: during first two weeks of the quarter
1. Review previous quarter during board meeting: after the start of the quarter

## Critical acclaim

- As the worlds biggest OKR critic, This is such a step in the right direction :heart: 10 million thumbs up
- I like it too, especially the fact that it is in one page, and that it stops at the team level.
- I like: stoping at the team level, clear reporting structure that isn't weekly, limiting KRs to 9 per team vs 3 per team and 3 per each IC.
- I've been working on a satirical blog post called called "HOT NEW MANAGEMENT TREND ALERT: RJGs: Really Just Goals" and this is basically that. :wink: Most of these are currently just KPIs but I won't say that too loudly :wink: It also embodies my point from that OKR hit piece: "As team lead, it’s your job to know your team, to keep them accountable to you, and themselves, and to be accountable for your department to the greater company. Other departments shouldn’t care about how you measure internal success or work as a team, as long as the larger agreed upon KPIs are aligned and being met."
- I always felt like OKRs really force every person to limit freedom to prioritize and limit flexibility. These ones fix that!

## 2017-Q3 DRAFT

Summary: With a great team make a popular next generation product that grows revenue.

TODO: Link to Diff with the start of the quarter

Note: The CMO goals are only proposals.

### Company: Grow incremental ACV according to plan.

* CEO: Build SQL$ pipeline for new business. 300% of IACV plan
  * CMO: Identify CE installations within Fortune 1000 with installations with over 500 users and launch demand generation campaigns.
    * SDR lead: Get an SQL from Fortune 1000 companies. 50% of companies.
    * SDR lead: Increase success rate. 20%
  * CFO: Salesforce shows relevant sales and activity. Zuora and Zendesk integrated
  * VP Prod: Salesforce shows relevant usage. [Version check](https://gitlab.com/gitlab-com/salesforce/issues/104), usage ping, and ConvDev index.
  * CRO: Increase new business sales velocity. Grow xxx% YoY to 479 per quarter.
    * Federal: Program for largest government facility.
    * Federal: Government facility becomes a customer.
    * APEC: Get 2 big vendors as resellers. Esprit and Beetle.
  * CMO: More demand coming from inbound marketing activities. 50% of sales quota.
  * CMO: Increase EE Trials. 50% QoQ
  * CRO: Improve trial conversion. 50% QoQ
* CEO: Understand and improve existing account growth. 200% YoY
  * VP Eng: Geo DR successful deployments. Test fail-over with GitLab.com.
    * Platform: Get Geo reliable with a testing framework.
    * VP Prod: Make ConvDev index a great experience.
  * VP Product: Ensure people are aware of EE features. [Make it easier to discover and use EE features](https://gitlab.com/gitlab-org/gitlab-ee/issues/2417).
    * Technical Writing: CE docs link to EE wherever relevant.
  * VP Eng: Fix things that cause churn. (Sid: make this actionable or deprecate).
  * CMO: Educational Email Campaign Series to market what is there and how to get started. 3 campaigns.
  * Customer Success: Get the two deals that were detailed closed.
  * Customer Success: Get Fable users of deprecated server to migrate.
  * Customer Success: Every significant renewal is triaged.
* CEO: Increase ASP (IACV per won) by 25% QoQ
    * CRO: 50% of ACV comes from EEP deals
    * CRO: Double average deal size YoY to $3,741
    * CRO: Double Q2 New Strategic/Large New Logos to 46
    * CMO: Demo's easy to give. 75% of company passes test.
    * CMO: 2 ROI calculators
    * CMO: 3 case studies
    * VP Prod: Conversational Development Index in product and SFDC
    * CMO: Presentation generation based on conversational development index.
    * VP Prod: Improve JIRA support. Better than BitBucket.

### Company: Popular next generation product.

* CEO: GitLab.com ready for mission-critical tasks
  * VP Scaling: Highly Available. [99.9%](http://stats.pingdom.com/81vpf8jyr1h9/1902794/history)
    * Production: HA setup for Redis
    * Production: Robust backups (Backups automated, and path to restore from backup tested and automated.)
    * Production: Canary deployments enabled
    * Platform: Use Geo DR to move between clouds and AZs
    * Database: PostgreSQL fail-over. Tested
    * Database: Zero downtime migration. Shipped
    * Production: Enable Elastic Search. 99% available
    * Support: Make features required. Don't use terminal anymore
    * Prometheus: Allow to monitor Unicorn. Deprecate InfluxDB
  * VP Scaling: [Lower latency](https://gitlab.com/gitlab-com/infrastructure/issues/947). [99% of user requests < 1 second](https://performance.gitlab.net/dashboard/db/transaction-overview?panelId=2&fullscreen&orgId=1)
    * Gitaly: Gitaly active on file-servers
    * Database: All slow queries have issues.
    * Discussion: Solve performance issues. Close 100%
    * Platform: Solve performance issues. Close 100%
  * VP Scaling: Secure platform
    * Security: Improve defenses. Implement top 10.
    * Security: Vulnerability testing. Conduct external test.
* CEO: Increased usage of idea to production. 100%
  * CMO: More awareness. [Overtake BitBucket in Google Trends](https://trends.google.com/trends/explore?q=bitbucket,gitlab).
  * VP Prod: Issue boards usage increase. 100% (.com and usage ping)
  * VP Prod: Service Desk usage increase. 100% (.com and usage ping)
  * Head Prod: Monitoring usage increase. 100% (.com and usage ping)
    * Prometheus: [Have many metrics enabled by default](https://gitlab.com/gitlab-org/gitlab-ce/issues/30369).
    * Prometheus: [Single click deploy of Prometheus](https://gitlab.com/gitlab-org/gitlab-ce/issues/28916)
  * Head Prod: Pipelines usage increase. 100% (.com and usage ping)
    * CI/CD: [Improve onboarding](https://gitlab.com/gitlab-org/gitlab-ce/issues/32638). 10 issues shipped.
    * CI/CD: [One-click Spring, Node, Ruby templates](https://gitlab.com/gitlab-org/gitlab-ce/issues/32420). Shipped.
  * Head Prod: Environments usage increase. 100% (.com and usage ping)
  * Head Prod: Review apps usage increase. 100% (.com and usage ping)
  * CTO: Automatic code quality monitoring. Shipped.
  * CTO: PGP signing support. Shipped.
  * CTO: Improve code quality and test coverage. 20 merge requests.
  * CMO: Better explain our features. Flow page that links to feature pages.
  * Build: [Lets Encrypt by default.](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/1096) In Omnibus and Helm. For Rails app, container registry, and pages.
  * UX: Improved navigation. Iterate on it every month.
  * Head Prod: [Make vision video](https://gitlab.com/gitlab-org/gitlab-ce/issues/32640). Published.
  * Head Prod: Marketing copy for [Auto DevOps](https://gitlab.com/gitlab-org/gitlab-ee/issues/2517). Published.
  * Head Prod: [Auto DevOps](https://gitlab.com/gitlab-org/gitlab-ee/issues/2517) plans. 10 issues worked out.
  * Dir Partnerships: Get projects to move. Drupal, Gnome, Kubernetes.
  * Dir Partnerships: Get major partner to use it for CI.
  * Dir Partnerships: Get Kubernetes distributors to recommend us.
  * CI/CD: [Java artifact repository](https://gitlab.com/gitlab-org/gitlab-ce/issues/19095). Maven compatible first iteration.
  * CMO: Wide read tutorials for Rails, Node, and Spring. 50 retweets each
  * VP Prod: 5 minute installation with all features (mail, container registry, etc.)
* CEO: Using our own features for GitLab.com
  * VP Eng: Make sure existing features are used
    * Production: GitLab.com uses Kubernetes. In use.
    * Production: GitLab.com uses Canary deploys. In use.
    * Production: GitLab.com uses Review apps. In use.
    * Support: GitLab.com uses Service Desk. In use for one process.
    * Build: GitLab.com uses Helm. In use.
    * CI/CD: Help GitLab.com use our deployment features. CD/Kubernetes/Helm/Canary deploys/Review Apps/Service Desk
    * Edge: GDK based on minikube
  * Head Prod: Ensure all features are usable by GitLab.com

### Company: Great team.

* CEO: Effective leadership. NPS of leaders 20%
  * Sr.Dir.PO: 75% of management certified in standard classes.
  * Sr.Dir.PO: 5 custom management classes given by Sid.
  * CFO: [Real-time analytics platform](https://about.gitlab.com/handbook/finance/analytics/). 40% of metrics
* CEO: Attract great people. Hires score 10%
  * Sr.Dir.PO: Outbound recruiting getting. 30% of total.
  * Sr.Dir.PO: Great recruiting process. NPS 20%
  * Sr.Dir.PO: Structured interview questions. Implemented.
  * Sr.Dir.PO: Interviewer rating. Implemented
  * Sr.Dir.PO: Quicker hiring cycle. Hiring time -25%
  * Sr.Dir.PO: Improve our pay structure. Base geo-diff on better data.
* CEO: Retain great people. eNPS of individual contributors 10%
  * CRO: Sales training program to advance sales skills. 80% of sales people trained.
  * Sr.Dir.PO: Improve diversity. One initiative deployed.
