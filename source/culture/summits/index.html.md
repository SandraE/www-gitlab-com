---
layout: markdown_page
title: "GitLab Summits"
---

- TOC
{:toc}

## What, where, and when
{: #what-when-where}

We try to get together every 9 months or so to get face-time, build community, 
and get some work done! Since our team is scattered all over the globe, we try to 
plan a different location for each Summit.

## Who
{: #who}

All [GitLab Team](/team/), their significant others, and the [Core Team](/core-team/).

## Previous Summits
{: #previous-summits}

### Summit in Cancun, Mexico

In January 2017 we met in Cancun, Mexico, where roughly 150 team members and 50 
significant others flew in from 35 different countries.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/XDfTj8iv9qw" frameborder="0" allowfullscreen="true"> </iframe>
</figure>


### Summit in Austin, TX, the USA

In May 2016, our team of 85 met up in Austin, TX to see if they were (still) as 
awesome as seen on Google Hangout.

Here's some footage our team put together to show how much fun we had.

<figure class="video_container">
  <iframe src="https://player.vimeo.com/video/175270564" frameborder="0" allowfullscreen="true"> </iframe>
</figure>


### Summit in Amsterdam, the Netherlands

Here are some impressions from our [first Summit](https://about.gitlab.com/2015/11/30/gitlab-summit-2015/) in October 2015.

<br>


## General information
{: #general-info}

* Significant Others (SOs for short) are very welcome, one per team member
* You are responsible for the SO you invite
* SOs should do their best to get to know many people
* SOs are welcome at all presentations, events, and meetings that are open to 
all team members
* If you're having a meal with your SO, pick a table with more than two seats 
so you can invite others to join you

Each Summit is intended to profoundly promote collaboration and understanding 
between GitLab contributors. Spouses are welcome to attend, but their presence 
should not distract team members from engaging with their peers and actively 
participating in Summit activities with the team. 

Children are strongly discouraged from attending. We have observed from past Summits 
that contributors who have chosen to bring children spend significantly less time 
collaborating with GitLab coworkers. Accordingly, we are requesting that if you 
must travel with family members you fully engage in as many group activities as 
you are physically able, invite team members to join you during meals, attend all 
company meetings, and recognize that the Summit is an investment in the continued 
growth of the organization. 

The goal of our Summits is to get to know each other outside of work.
To help get this kickstarted, always wear your nametag to all events on all days. 
Try to join different people every time we sit down for a meal.

{:.alert .alert-info}

* Email/file templates for project page _(to be included)_
* Audience is team members, applicants, SO's, partner companies, and wider community

### Health and safety
{: #health-and-safety}

* Use fistbumps instead of handshaking to avoid getting sick
* We don't accept any form of (sexual) harassment
* We don't accept unbecoming behaviour from our team members nor their SOs (e.g. 
don't get drunk, do drugs, or peer pressure people into drinking alcohol)
* When there is unbecoming behaviour (e.g if someone does get drunk) bring them 
to their room
* Don't use the pool or go swimming after midnight
* Be respectful of other hotel guests (e.g. don't talk on your floor when returning 
to your room & keep your volume down at restaurants/bars)

### Important logistical for every Summit
{: #important-basics}

* Ensure there is great wifi -- we need more serious tech for the main room (5GHz, multiple points)
* Ensure there are large meeting rooms for team members to join work hours and presentations
* Label your charger with your name for easy return to owner in case you lose it

### Presentations
{: #presentations}

The following should be assigned and/or arranged a month before the Summit:

* All interviews, presentations and content production
* Who will be presenting and when & where they will be presenting
* Projectors, wireless (non-handheld) microphones, and any other (audio) needs 
* Recording equipment such as stage cam, audience cam, presentation feed etc.
* An audio feed that goes directly from microphone into the recording
* A program and manager for live streaming
* The blog text for the presentation, including relevant materials shared after 
the presentation, as well as approval and a plan to publish the blog 24 hours after the presentation is given

